# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-08 11:57-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: reportes_app/actividad_agente_log.py:131
msgid "1 día, "
msgstr ""

#: reportes_app/actividad_agente_log.py:133
msgid " días, "
msgstr ""

#: reportes_app/apps.py:33
#: reportes_app/templates/reporte_grafico_campana.html:273
#: reportes_app/templates/reporte_llamadas.html:33
msgid "Llamadas"
msgstr ""

#: reportes_app/apps.py:38
#: reportes_app/templates/reportes_agentes_tiempos.html:36
msgid "Agentes"
msgstr ""

#: reportes_app/apps.py:45
msgid "Reportes"
msgstr ""

#: reportes_app/apps.py:106
msgid "Reportes de tiempos de agentes (sesión, pausa, llamada, etc...)"
msgstr ""

#: reportes_app/apps.py:109
msgid "Exporta reportes de agentes."
msgstr ""

#: reportes_app/apps.py:111
msgid "Reporte de tiempos de agente para ventana modal."
msgstr ""

#: reportes_app/apps.py:114
msgid "Reportes de pausas de agente para ventana modal."
msgstr ""

#: reportes_app/apps.py:117
msgid "Historico de llamadas para mostrar en la consola de Agente"
msgstr ""

#: reportes_app/apps.py:120
msgid "Reporte de Llamadas"
msgstr ""

#: reportes_app/apps.py:122
msgid "Descarga csv de reporte de llamadas"
msgstr ""

#: reportes_app/apps.py:124
msgid "Descarga .zip de reporte de llamadas"
msgstr ""

#: reportes_app/apps.py:126
msgid "Reporte de calificaciones de campaña"
msgstr ""

#: reportes_app/apps.py:128
msgid "Descargar Reporte de calificaciones de campaña"
msgstr ""

#: reportes_app/apps.py:131
msgid "Reporte Grafico de campaña"
msgstr ""

#: reportes_app/apps.py:133
msgid "Descargar Reporte Grafico de campaña"
msgstr ""

#: reportes_app/apps.py:135
msgid "Reporte Grafico de Agente en Campana"
msgstr ""

#: reportes_app/apps.py:137
msgid "Reporte de gestiones de un Agente"
msgstr ""

#: reportes_app/apps.py:139
msgid "Descargar Reporte de llamados contactados de una campaña"
msgstr ""

#: reportes_app/apps.py:142
msgid "Descargar Reporte de llamados calificados de una campaña"
msgstr ""

#: reportes_app/apps.py:145
msgid "Descargar Reporte de llamados no atendidos de una campaña"
msgstr ""

#: reportes_app/apps.py:148
msgid "Detalle del estado de una campaña Preview"
msgstr ""

#: reportes_app/apps.py:150
msgid "Detalle del estado de una campaña Preview (para ventana modal)"
msgstr ""

#: reportes_app/apps.py:153
msgid "Detalle del estado de una campaña Dialer"
msgstr ""

#: reportes_app/apps.py:155
msgid "Detalle del estado de una campaña Dialer (para ventana modal)"
msgstr ""

#: reportes_app/apps.py:158
msgid "Reporte de resultado de contactaciones en una Campaña"
msgstr ""

#: reportes_app/apps.py:161
msgid "Descargar reporte de resultado de contactaciones en una Campaña"
msgstr ""

#: reportes_app/apps.py:164
msgid "Descargar reporte de todas las contactaciones en una Campaña"
msgstr ""

#: reportes_app/archivos_de_reporte/reporte_de_resultados.py:76
#: reportes_app/templates/calificaciones_campana.html:38
#: reportes_app/templates/calificaciones_campana.html:77
#: reportes_app/templates/reporte_agente.html:72
#: reportes_app/templates/reporte_de_resultados.html:34
msgid "Calificación"
msgstr ""

#: reportes_app/archivos_de_reporte/reporte_de_resultados.py:77
#: reportes_app/templates/reporte_de_resultados.html:35
msgid "Contactación"
msgstr ""

#: reportes_app/forms.py:60
msgid "Incluir campañas finalizadas"
msgstr ""

#: reportes_app/forms.py:69
msgid "Formato inválido"
msgstr ""

#: reportes_app/forms.py:83
msgid "Formato JSON invalido"
msgstr ""

#: reportes_app/models.py:135 reportes_app/models.py:147
msgid "No se encontraron llamadas "
msgstr ""

#: reportes_app/models.py:281
msgid "No se encontraron holds "
msgstr ""

#: reportes_app/models.py:424
msgid "Llamada manual"
msgstr ""

#: reportes_app/models.py:425
msgid "Llamada dialer"
msgstr ""

#: reportes_app/models.py:426
msgid "Llamada entrante"
msgstr ""

#: reportes_app/models.py:427
msgid "Llamada preview"
msgstr ""

#: reportes_app/models.py:428
msgid "Llamada click2call"
msgstr ""

#: reportes_app/models.py:429
msgid "Llamada transferencia interna"
msgstr ""

#: reportes_app/models.py:430
msgid "Llamada transferencia externa"
msgstr ""

#: reportes_app/models.py:472
msgid "No se encontraron pausas "
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos.py:353
#: reportes_app/reportes/reporte_agentes.py:285
msgid "ACW"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos.py:356
msgid "Supervisión"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:62
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:468
#, python-brace-format
msgid ""
"ArchivoDeReporteCsv: Ya existe archivo CSV de reporte para la campana {0}. "
"Archivo: {1}. El archivo sera sobreescrito"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:78
#: reportes_app/reportes/reporte_agente_tiempos_csv.py:144
#: reportes_app/reportes/reporte_agente_tiempos_csv.py:177
#: reportes_app/reportes/reporte_agente_tiempos_csv.py:210
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:160
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:298
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:411
#: reportes_app/templates/reporte_agente.html:30
#: reportes_app/templates/reporte_grafico_campana.html:233
#: reportes_app/templates/reportes_agentes_tiempos.html:53
#: reportes_app/templates/reportes_agentes_tiempos.html:88
#: reportes_app/templates/reportes_agentes_tiempos.html:173
#: reportes_app/templates/reportes_agentes_tiempos.html:205
#: reportes_app/templates/reportes_agentes_tiempos.html:236
#: reportes_app/templates/reportes_agentes_tiempos.html:280
#: reportes_app/templates/reportes_agentes_tiempos.html:318
msgid "Agente"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:79
msgid "Tiempo de sesion"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:80
#: reportes_app/templates/reportes_agentes_tiempos.html:90
msgid "Tiempo de hold"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:81
#: reportes_app/reportes/reporte_agente_tiempos_csv.py:147
msgid "Tiempo de pausa"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:82
msgid "Tiempos en llamada"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:83
#: reportes_app/templates/reporte_agente.html:114
#: reportes_app/templates/reportes_agentes_tiempos.html:91
msgid "Porcentaje en llamada"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:84
#: reportes_app/templates/reporte_agente.html:115
msgid "Porcentaje en pausa"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:85
#: reportes_app/templates/reporte_agente.html:116
msgid "Porcentaje en espera"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:86
#: reportes_app/reportes/reporte_agente_tiempos_csv.py:180
#: reportes_app/templates/reporte_agente.html:117
msgid "Cantidad de llamadas procesadas"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:87
#: reportes_app/templates/reporte_agente.html:118
msgid "Tiempo promedio de llamadas"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:88
#: reportes_app/templates/reporte_agente.html:119
msgid "Cantidad de intentos fallidos"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:145
#: reportes_app/templates/reportes_agentes_tiempos.html:174
#: reportes_app/templates/reportes_agentes_tiempos.html:293
msgid "Pausa"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:146
msgid "Tipo de pausa"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:178
#: reportes_app/templates/reportes_agentes_tiempos.html:206
msgid "Cola"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:179
#: reportes_app/templates/reportes_agentes_tiempos.html:207
msgid "Tiempo de llamadas"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:211
#: reportes_app/reportes/reporte_llamadas.py:546
#: reportes_app/reportes/reporte_llamadas.py:664
#: reportes_app/reportes/reporte_llamadas.py:711
#: reportes_app/templates/reporte_llamadas.html:250
#: reportes_app/templates/reporte_llamadas.html:675
#: reportes_app/templates/reporte_llamadas.html:710
#: reportes_app/templates/reporte_llamadas.html:748
#: reportes_app/templates/reporte_llamadas.html:784
#: reportes_app/templates/reporte_llamadas.html:818
#: reportes_app/templates/reportes_agentes_tiempos.html:237
msgid "Total"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:212
msgid "ICS"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:213
#: reportes_app/templates/reportes_agentes_tiempos.html:239
msgid "DIALER"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:214
#: reportes_app/templates/reportes_agentes_tiempos.html:240
msgid "INBOUND"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:215
#: reportes_app/templates/reportes_agentes_tiempos.html:241
msgid "MANUAL"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:216
#: reportes_app/templates/reportes_agentes_tiempos.html:242
msgid "TRANSFERIDAS"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:217
#: reportes_app/templates/reportes_agentes_tiempos.html:243
msgid "FUERA DE CAMPAÑA"
msgstr ""

#: reportes_app/reportes/reporte_agente_tiempos_csv.py:282
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:501
#, python-brace-format
msgid ""
"obtener_url_reporte_csv_descargar(): NO existe archivo CSV de descarga para "
"la campana {0}"
msgstr ""

#: reportes_app/reportes/reporte_agentes.py:286
msgid "Pausa-Supervisión"
msgstr ""

#: reportes_app/reportes/reporte_estadisticas_agentes.py:56
#, python-brace-format
msgid "{0} hs"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:42
#: reportes_app/reportes/reporte_llamadas.py:474
#: reportes_app/reportes/reporte_llamadas.py:531
msgid "Transferencias"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:473
#: reportes_app/reportes/reporte_llamadas.py:547
#: reportes_app/reportes/reporte_llamadas.py:711
#: reportes_app/templates/reporte_llamadas.html:251
#: reportes_app/templates/reporte_llamadas.html:294
#: reportes_app/templates/reporte_llamadas.html:403
#: reportes_app/templates/reporte_llamadas.html:580
msgid "Manuales"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:473
#: reportes_app/reportes/reporte_llamadas.py:528
msgid "Dialer"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:473
msgid "Entrantes"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:473
#: reportes_app/reportes/reporte_llamadas.py:530
msgid "Preview"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:477
msgid "Intentos"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:485
#: reportes_app/templates/reporte_llamadas.html:293
#: reportes_app/templates/reporte_llamadas.html:508
#: reportes_app/templates/reporte_llamadas.html:579
#: reportes_app/templates/reporte_llamadas.html:581
msgid "Conexión"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:498
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:55
msgid "Fallo"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:508
msgid "No hay llamadas para ese periodo"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:527
msgid "Manual"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:529
msgid "Entrante"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:567
msgid "No contactadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:568
msgid "Procesadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:569
#: reportes_app/reportes/reporte_llamadas.py:591
#: reportes_app/reportes/reporte_llamadas.py:666
#: reportes_app/reportes/reporte_llamadas.py:736
#: reportes_app/reportes/reporte_llamadas.py:758
#: reportes_app/templates/reporte_llamadas.html:116
#: reportes_app/templates/reporte_llamadas.html:746
msgid "Abandonadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:570
#: reportes_app/reportes/reporte_llamadas.py:593
#: reportes_app/reportes/reporte_llamadas.py:665
#: reportes_app/reportes/reporte_llamadas.py:735
#: reportes_app/reportes/reporte_llamadas.py:758
#: reportes_app/templates/reporte_llamadas.html:112
#: reportes_app/templates/reporte_llamadas.html:745
msgid "Expiradas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:590
#: reportes_app/reportes/reporte_llamadas.py:665
#: reportes_app/reportes/reporte_llamadas.py:735
#: reportes_app/reportes/reporte_llamadas.py:758
#: reportes_app/templates/reporte_llamadas.html:82
#: reportes_app/templates/reporte_llamadas.html:108
#: reportes_app/templates/reporte_llamadas.html:672
#: reportes_app/templates/reporte_llamadas.html:744
msgid "Atendidas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:592
#: reportes_app/reportes/reporte_llamadas.py:666
#: reportes_app/reportes/reporte_llamadas.py:759
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:62
#: reportes_app/templates/reporte_llamadas.html:120
#: reportes_app/templates/reporte_llamadas.html:747
msgid "Abandonadas durante anuncio"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:609
#: reportes_app/reportes/reporte_llamadas.py:627
#: reportes_app/reportes/reporte_llamadas.py:664
#: reportes_app/reportes/reporte_llamadas.py:722
#: reportes_app/reportes/reporte_llamadas.py:735
#: reportes_app/reportes/reporte_llamadas.py:780
#: reportes_app/templates/reporte_llamadas.html:138
#: reportes_app/templates/reporte_llamadas.html:160
#: reportes_app/templates/reporte_llamadas.html:182
#: reportes_app/templates/reporte_llamadas.html:708
#: reportes_app/templates/reporte_llamadas.html:782
#: reportes_app/templates/reporte_llamadas.html:816
msgid "Conectadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:610
#: reportes_app/reportes/reporte_llamadas.py:628
#: reportes_app/reportes/reporte_llamadas.py:664
#: reportes_app/reportes/reporte_llamadas.py:722
#: reportes_app/reportes/reporte_llamadas.py:780
msgid "No conectadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:664
#: reportes_app/reportes/reporte_llamadas.py:711
#: reportes_app/templates/reporte_llamadas.html:252
#: reportes_app/templates/reportes_agentes_tiempos.html:175
msgid "Tipo"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:665
#: reportes_app/templates/reporte_llamadas.html:86
#: reportes_app/templates/reporte_llamadas.html:673
msgid "No atendidas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:665
#: reportes_app/templates/reporte_llamadas.html:90
#: reportes_app/templates/reporte_llamadas.html:674
msgid "Perdidas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:711
#: reportes_app/reportes/reporte_llamadas.py:722
#: reportes_app/reportes/reporte_llamadas.py:735
#: reportes_app/reportes/reporte_llamadas.py:758
#: reportes_app/reportes/reporte_llamadas.py:780
#: reportes_app/templates/reporte_llamadas.html:400
#: reportes_app/templates/reporte_llamadas.html:506
#: reportes_app/templates/reporte_llamadas.html:577
msgid "Nombre"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:722
#: reportes_app/reportes/reporte_llamadas.py:735
#: reportes_app/reportes/reporte_llamadas.py:780
#: reportes_app/templates/reporte_llamadas.html:292
#: reportes_app/templates/reporte_llamadas.html:507
#: reportes_app/templates/reporte_llamadas.html:578
msgid "Efectuadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:723
#: reportes_app/reportes/reporte_llamadas.py:737
#: reportes_app/reportes/reporte_llamadas.py:759
#: reportes_app/reportes/reporte_llamadas.py:781
msgid "T. Espera Conexion"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:736
#: reportes_app/reportes/reporte_llamadas.py:759
msgid "T. Abandono"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:736
msgid "T. Espera Atención"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:737
#: reportes_app/reportes/reporte_llamadas.py:760
#: reportes_app/reportes/reporte_llamadas.py:781
msgid "Manuales Efectuadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:737
#: reportes_app/reportes/reporte_llamadas.py:760
#: reportes_app/reportes/reporte_llamadas.py:781
msgid "Manuales Conectadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:738
#: reportes_app/reportes/reporte_llamadas.py:760
#: reportes_app/reportes/reporte_llamadas.py:782
msgid "Manuales No Conectadas"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:738
#: reportes_app/reportes/reporte_llamadas.py:761
#: reportes_app/reportes/reporte_llamadas.py:782
msgid "T. Espera Conexión Manuales"
msgstr ""

#: reportes_app/reportes/reporte_llamadas.py:758
#: reportes_app/templates/reporte_llamadas.html:401
msgid "Recibidas"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:50
msgid "Cliente no atiende"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:51
msgid "Se corta antes que atienda el cliente"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:52
msgid "Ocupado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:53
msgid "Canales Saturados"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:54
msgid "Motivo no especificado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:56
msgid "Contestador"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:57
msgid "Blacklist"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:58
msgid "Abandonada por cliente"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:59
msgid "Expirada"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:60
msgid "Canal congestionado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:61
msgid "Problema de enrutamiento"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:125
msgid "Llamada Atendida sin calificacion"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:137
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:138
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:344
msgid "Fuera de base"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:143
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:335
#: reportes_app/reportes/reporte_resultados.py:113
#: reportes_app/tests/tests_reporte_resultados.py:65
msgid "Contactado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:151
msgid "Telefono contactado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:155
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:293
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:409
msgid "Fecha-Hora Contacto"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:156
msgid "Duración"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:157
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:294
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:410
msgid "Tel status"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:158
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:296
msgid "Calificado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:159
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:297
#: reportes_app/templates/calificaciones_campana.html:39
#: reportes_app/templates/calificaciones_campana.html:78
msgid "Observaciones"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:161
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:299
msgid "base de datos"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:162
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:300
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:412
msgid "Callid"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:163
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:301
#: reportes_app/reportes/reporte_llamados_contactados_csv.py:413
msgid "Tipo llamada"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:295
msgid "Tel contactado"
msgstr ""

#: reportes_app/reportes/reporte_llamados_contactados_csv.py:407
msgid "Telefono de llamada"
msgstr ""

#: reportes_app/reportes/reporte_resultados.py:115
msgid "Sin datos"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:22
#: reportes_app/templates/reporte_llamadas.html:249
#: reportes_app/templates/reporte_llamadas.html:291
msgid "Campaña"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:25
msgid "Desde:"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:26
msgid "Hasta:"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:28
msgid "Base Datos:"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:30
msgid "Exportar Calificación(CSV)"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:31
msgid "Exportar Gestión:"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:36
#: reportes_app/templates/reporte_de_resultados.html:30
msgid "Teléfono"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:37
msgid "Datos"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:70
msgid "Histórico de calificaciones"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:76
#: reportes_app/templates/reporte_llamadas.html:48
#: reportes_app/templates/reporte_llamadas.html:671
#: reportes_app/templates/reporte_llamadas.html:707
#: reportes_app/templates/reporte_llamadas.html:743
#: reportes_app/templates/reporte_llamadas.html:781
#: reportes_app/templates/reporte_llamadas.html:815
#: reportes_app/templates/reportes_agentes_tiempos.html:48
#: reportes_app/templates/reportes_agentes_tiempos.html:290
#: reportes_app/templates/reportes_agentes_tiempos.html:327
msgid "Fecha"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:76
msgid "Hora"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:79
#: reportes_app/templates/reporte_grafico_campana.html:234
msgid "Gestiones"
msgstr ""

#: reportes_app/templates/calificaciones_campana.html:103
msgid "No se encontraron Calificaciones para esta campaña."
msgstr ""

#: reportes_app/templates/reporte_agente.html:47
#: reportes_app/templates/reporte_grafico_campana.html:90
msgid "Filtrar por esa fecha"
msgstr ""

#: reportes_app/templates/reporte_agente.html:62
msgid "Cantidad de calificaciones por el agente"
msgstr ""

#: reportes_app/templates/reporte_agente.html:73
msgid "cantidad"
msgstr ""

#: reportes_app/templates/reporte_agente.html:90
#: reportes_app/templates/reporte_grafico_campana.html:137
msgid "Total asignados"
msgstr ""

#: reportes_app/templates/reporte_agente.html:110
msgid "Tiempo en sesión"
msgstr ""

#: reportes_app/templates/reporte_agente.html:111
msgid "Tiempo en hold"
msgstr ""

#: reportes_app/templates/reporte_agente.html:112
msgid "Tiempo en pausa"
msgstr ""

#: reportes_app/templates/reporte_agente.html:113
msgid "Tiempo en llamada"
msgstr ""

#: reportes_app/templates/reporte_agente.html:144
msgid "Volver a reporte campana"
msgstr ""

#: reportes_app/templates/reporte_de_resultados.html:21
msgid "Estado de contactaciones de la Campaña: "
msgstr ""

#: reportes_app/templates/reporte_de_resultados.html:23
msgid "Exportar Contactaciones"
msgstr ""

#: reportes_app/templates/reporte_de_resultados.html:25
msgid "Contabilizar todos los contactados"
msgstr ""

#: reportes_app/templates/reporte_de_resultados.html:51
msgid "No se encontraron Contactaciones para esta campaña."
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:40
msgid "Campaña:"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:46
msgid "Llamadas Pendientes"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:52
msgid "Llamadas Realizadas"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:58
msgid "Llamadas Recibidas"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:64
msgid "Tiempo promedio de espera (en segundos)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:70
msgid "Tiempo promedio de abandono (en segundos)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:75
msgid "Exportar Reporte(PDF)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:80
#: reportes_app/templates/reporte_llamadas.html:42
#: reportes_app/templates/reporte_llamadas.html:58
#: reportes_app/templates/reportes_agentes_tiempos.html:39
#: reportes_app/templates/reportes_agentes_tiempos.html:70
msgid "Buscar"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:97
msgid "Reportes de contactados"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:101
msgid "Generar Reporte de contactados (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:102
msgid "Descargar Reporte de contactados (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:104
#: reportes_app/templates/reporte_grafico_campana.html:156
#: reportes_app/templates/reporte_grafico_campana.html:212
msgid ""
"ADVERTENCIA: generar un CSV de mas de un mes mientras estan operando los "
"agentes puede generar lentitud en el sistema"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:118
msgid "Llamadas conectadas al agente"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:125
msgid "Calificacion"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:126
#: reportes_app/templates/reporte_grafico_campana.html:181
#: reportes_app/templates/reporte_grafico_campana.html:274
msgid "Cantidad"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:153
msgid "Generar Reporte de calificados (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:154
msgid "Descargar Reporte de calificados (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:173
msgid "Cantidad de llamados no atendidos"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:180
msgid "Resultado"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:192
msgid "Total no atendidos"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:209
msgid "Generar Reporte de no atendidos (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:210
msgid "Descargar Reporte de no atendidos (CSV)"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:227
msgid "Calificaciones por Agente"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:238
msgid "Contactos calificados"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:254
msgid "Totales"
msgstr ""

#: reportes_app/templates/reporte_grafico_campana.html:266
msgid "Detalle de las llamadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:64
msgid "Período"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:70
#: reportes_app/templates/reportes_agentes_tiempos.html:92
#: reportes_app/templates/reportes_agentes_tiempos.html:208
#: reportes_app/templates/reportes_agentes_tiempos.html:298
msgid "Llamadas procesadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:78
msgid "Llamadas salientes por Discador"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:96
#: reportes_app/templates/reporte_llamadas.html:126
#: reportes_app/templates/reporte_llamadas.html:148
#: reportes_app/templates/reporte_llamadas.html:170
#: reportes_app/templates/reporte_llamadas.html:192
msgid "Desglosar por fecha"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:104
msgid "Llamadas Entrantes"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:134
msgid "Llamadas salientes Manuales"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:142
#: reportes_app/templates/reporte_llamadas.html:164
#: reportes_app/templates/reporte_llamadas.html:186
#: reportes_app/templates/reporte_llamadas.html:332
#: reportes_app/templates/reporte_llamadas.html:440
#: reportes_app/templates/reporte_llamadas.html:471
#: reportes_app/templates/reporte_llamadas.html:528
#: reportes_app/templates/reporte_llamadas.html:542
#: reportes_app/templates/reporte_llamadas.html:601
#: reportes_app/templates/reporte_llamadas.html:610
#: reportes_app/templates/reporte_llamadas.html:624
#: reportes_app/templates/reporte_llamadas.html:633
#: reportes_app/templates/reporte_llamadas.html:709
#: reportes_app/templates/reporte_llamadas.html:783
#: reportes_app/templates/reporte_llamadas.html:817
msgid "no conec."
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:156
msgid "Llamadas salientes Preview"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:178
msgid "Llamadas Transferidas a Campañas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:202
msgid "Exportar totales llamadas(CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:209
msgid "Exportar a CSV todos los reportes"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:219
msgid "Totales por tipos de llamadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:223
msgid "Totales de llamadas por tipo de llamada y forma de finalización"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:231
msgid "Porcentaje de llamadas por tipo de llamada"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:243
msgid "Cantidad de llamadas por campaña"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:268
msgid "Cantidad de llamadas de las campañas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:277
msgid "Exportar llamadas por campaña (CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:283
msgid "Distribución de tipos de llamadas en campañas salientes discador"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:286
#: reportes_app/templates/reporte_llamadas.html:395
#: reportes_app/templates/reporte_llamadas.html:501
#: reportes_app/templates/reporte_llamadas.html:572
#: reportes_app/templates/reportes_agentes_tiempos.html:82
msgid "Información completa"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:302
#: reportes_app/templates/reporte_llamadas.html:411
#: reportes_app/templates/reporte_llamadas.html:516
#: reportes_app/templates/reporte_llamadas.html:589
msgid "Ver llamadas por fecha"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:313
#: reportes_app/templates/reporte_llamadas.html:345
#: reportes_app/templates/reporte_llamadas.html:422
#: reportes_app/templates/reporte_llamadas.html:453
msgid "atendidas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:314
#: reportes_app/templates/reporte_llamadas.html:331
#: reportes_app/templates/reporte_llamadas.html:346
#: reportes_app/templates/reporte_llamadas.html:363
#: reportes_app/templates/reporte_llamadas.html:439
#: reportes_app/templates/reporte_llamadas.html:470
#: reportes_app/templates/reporte_llamadas.html:527
#: reportes_app/templates/reporte_llamadas.html:541
#: reportes_app/templates/reporte_llamadas.html:600
#: reportes_app/templates/reporte_llamadas.html:609
#: reportes_app/templates/reporte_llamadas.html:623
#: reportes_app/templates/reporte_llamadas.html:632
msgid "conectadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:315
#: reportes_app/templates/reporte_llamadas.html:347
#: reportes_app/templates/reporte_llamadas.html:423
#: reportes_app/templates/reporte_llamadas.html:454
msgid "expiradas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:316
#: reportes_app/templates/reporte_llamadas.html:348
#: reportes_app/templates/reporte_llamadas.html:424
#: reportes_app/templates/reporte_llamadas.html:455
msgid "abandonadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:323
#: reportes_app/templates/reporte_llamadas.html:355
msgid "atención"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:324
#: reportes_app/templates/reporte_llamadas.html:356
#: reportes_app/templates/reporte_llamadas.html:432
#: reportes_app/templates/reporte_llamadas.html:463
msgid "abandono"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:333
#: reportes_app/templates/reporte_llamadas.html:365
#: reportes_app/templates/reporte_llamadas.html:441
#: reportes_app/templates/reporte_llamadas.html:472
#: reportes_app/templates/reportes_agentes_tiempos.html:152
msgid "espera"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:364
msgid "no conectadas"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:377
msgid "Tipos de llamadas por campaña de discador"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:383
msgid "Exportar tipo de llamadas de Campañas Dialer (CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:392
msgid "Distribucion de tipos de llamadas en campañas entrantes"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:402
msgid "Espera"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:425
#: reportes_app/templates/reporte_llamadas.html:456
msgid "abandonadas en anuncio"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:484
msgid "Tipos de llamadas por campaña entrantes"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:490
msgid "Exportar tipo de llamadas de Campañas Entrantes (CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:498
msgid "Distribucion de tipos de llamadas en campañas manuales"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:555
msgid "Tipos de llamadas por campaña manuales"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:561
msgid "Exportar tipo de llamadas de Campañas Manuales (CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:569
msgid "Distribucion de tipos de llamadas en campañas Preview"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:646
msgid "Tipos de llamadas por campaña preview"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:652
msgid "Exportar tipo de llamadas de Campañas Preview (CSV)"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:664
msgid "Tipos de llamadas salientes por Discador"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:690
#: reportes_app/templates/reporte_llamadas.html:726
#: reportes_app/templates/reporte_llamadas.html:764
#: reportes_app/templates/reporte_llamadas.html:798
#: reportes_app/templates/reporte_llamadas.html:832
#: reportes_app/templates/reportes_agentes_tiempos.html:308
#: reportes_app/templates/reportes_agentes_tiempos.html:336
msgid "Cerrar"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:700
msgid "Tipos de llamadas salientes Manuales"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:736
msgid "Tipos de llamadas Entrantes"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:774
msgid "Tipos de llamadas salientes Preview"
msgstr ""

#: reportes_app/templates/reporte_llamadas.html:808
msgid "Tipos de llamadas de Transferencia"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:58
msgid "Todos los agentes"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:63
msgid "Grupo de agentes"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:79
msgid "Período:"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:89
msgid "Tiempo de session"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:93
#: reportes_app/templates/reportes_agentes_tiempos.html:299
msgid "Tiempo promedio"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:94
#: reportes_app/templates/reportes_agentes_tiempos.html:300
msgid "Intentos fallidos"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:104
#: reportes_app/templates/reportes_agentes_tiempos.html:185
msgid "ver por fechas"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:109
#: reportes_app/templates/reportes_agentes_tiempos.html:111
#: reportes_app/templates/reportes_agentes_tiempos.html:116
#: reportes_app/templates/reportes_agentes_tiempos.html:118
#: reportes_app/templates/reportes_agentes_tiempos.html:121
#: reportes_app/templates/reportes_agentes_tiempos.html:127
#: reportes_app/templates/reportes_agentes_tiempos.html:129
#: reportes_app/templates/reportes_agentes_tiempos.html:189
#: reportes_app/templates/reportes_agentes_tiempos.html:216
#: reportes_app/templates/tbody_fechas_agentes.html:28
#: reportes_app/templates/tbody_fechas_agentes.html:30
#: reportes_app/templates/tbody_fechas_agentes.html:35
#: reportes_app/templates/tbody_fechas_agentes.html:37
#: reportes_app/templates/tbody_fechas_agentes.html:42
#: reportes_app/templates/tbody_fechas_agentes.html:44
#: reportes_app/templates/tbody_fechas_agentes.html:47
#: reportes_app/templates/tbody_pausa_fechas_agentes.html:24
msgid "hs"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:120
#: reportes_app/templates/reportes_agentes_tiempos.html:146
msgid "pausa"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:122
msgid "llamada"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:157
#: reportes_app/templates/tbody_fechas_agentes.html:70
msgid "s"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:165
msgid "Exportar reporte de tiempos(CSV)"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:176
#: reportes_app/templates/reportes_agentes_tiempos.html:328
msgid "Tiempo"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:197
msgid "Exportar reporte de pausas(CSV)"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:226
msgid "Exportar reporte cantidad llamadas(CSV)"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:230
msgid "Cantidad de llamadas conectadas por agente"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:238
msgid "PREVIEW"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:263
msgid "Cantidad de llamadas de los agentes por tipo de llamadas"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:271
msgid "Exportar reporte de tipo de llamadas(CSV)"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:291
msgid "Sesión"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:292
msgid "Hold "
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:294
msgid "Llamada"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:295
#, python-format
msgid "%% En llamada"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:296
#, python-format
msgid "%% En pausa"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:297
#, python-format
msgid "%% En espera"
msgstr ""

#: reportes_app/templates/reportes_agentes_tiempos.html:318
msgid "para la pausa"
msgstr ""

#: reportes_app/views.py:217 reportes_app/views.py:236
msgid "No existe la campaña."
msgstr ""

#: reportes_app/views.py:225 reportes_app/views.py:246
msgid "Por favor, intente nuevamente."
msgstr ""

#: reportes_app/views_campanas_dialer_reportes.py:90
msgid ""
"No se pudo consultar el estado actual de la campaña. Consulte con su "
"administrador."
msgstr ""

#: reportes_app/views_reportes.py:65 reportes_app/views_reportes.py:197
msgid "Usted no puede acceder a esta campaña."
msgstr ""
